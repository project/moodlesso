<?php


function munge_drupal_authentication() {
    // if we're already  logged in, this doesn't matter.
    if (isloggedin()) {
        return false;
    }
    // if we've already tried, there's no point.
    global $SESSION;
    if (!empty($SESSION->triedsso)) {
        return false;
    }
    // if we haven't been at drupal already, don't bother.
    if (!isset($_COOKIE['MOODLEUSERNAME']) || !isset($_COOKIE['MOODLESSOTOKEN'])) {
        return false;
    }

    if (!$user = authenticate_user_login($_COOKIE['MOODLEUSERNAME'], $_COOKIE['MOODLESSOTOKEN'])) {
        $SESSION->triedsso = true;
        return false;
    }

    // if we've got this far, we've authenticated them!
    global $USER;
    $USER = $user;
    add_to_log(SITEID, 'user', 'drupal login', "view.php?id=$USER->id&course=".SITEID, $USER->id, 0, $USER->id);


    update_user_login_times();
    set_moodle_cookie($USER->username);
    set_login_session_preferences();

    /// This is what lets the user do anything on the site :-)
    load_all_capabilities();

    // if it's their first time and we don't know enough about them...
    if (user_not_fully_set_up($USER)) {
        global $CFG;
        redirect($CFG->wwwroot . '/user/edit.php');
    }
    return true;
}
?>
